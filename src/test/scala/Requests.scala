import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

object Requests {


  val uri1 = "https://openpaas.barcamp.awesomepaas.net"
  val uri2 = "https://syndication.twitter.com/settings"
  val uri4 = "https://platform.twitter.com/widgets/widget_iframe.a600a62a1c92aa33bb89e73fa1e8b3b3.html"
  val uri5 = "https://jmap.barcamp.awesomepaas.net/jmap"

  val login = http("request_0")
      .post("/?url=aHR0cHM6Ly9vcGVucGFhcy5iYXJjYW1wLmF3ZXNvbWVwYWFzLm5ldC8=")
      .headers(Headers.headers_0)
      .formParam("url", "aHR0cHM6Ly9vcGVucGFhcy5iYXJjYW1wLmF3ZXNvbWVwYWFzLm5ldC8=")
      .formParam("timezone", "1")
      .formParam("skin", "linagora")
      .formParam("user", "${user}")
      .formParam("password", "${password}")
      .resources(http("request_1")
        .get(uri1 + "/components/ng-tags-input/ng-tags-input.min.css")
        .headers(Headers.headers_1),
        http("request_2")
          .get(uri1 + "/components/bootstrap-additions/dist/bootstrap-additions.min.css")
          .headers(Headers.headers_2),
        http("request_3")
          .get(uri1 + "/components/angular-xeditable/dist/css/xeditable.css")
          .headers(Headers.headers_3),
        http("request_4")
          .get(uri1 + "/components/angular-motion/dist/angular-motion.min.css")
          .headers(Headers.headers_4),
        http("request_5")
          .get(uri1 + "/components/animate.css/animate.min.css")
          .headers(Headers.headers_5),
        http("request_6")
          .get(uri1 + "/components/lng-clockpicker/dist/bootstrap-clockpicker.min.css")
          .headers(Headers.headers_6),
        http("request_7")
          .get(uri1 + "/components/angular-chart.js/dist/angular-chart.css")
          .headers(Headers.headers_7),
        http("request_8")
          .get(uri1 + "/components/roboto-fontface/css/roboto-fontface.css")
          .headers(Headers.headers_8),
        http("request_9")
          .get(uri1 + "/components/summernote/dist/summernote.css")
          .headers(Headers.headers_9),
        http("request_10")
          .get(uri1 + "/components/jcrop/css/jquery.Jcrop.min.css")
          .headers(Headers.headers_10),
        http("request_11")
          .get(uri1 + "/components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css")
          .headers(Headers.headers_11),
        http("request_12")
          .get(uri1 + "/generated/js/esn/js/core")
          .headers(Headers.headers_12),
        http("request_13")
          .get(uri1 + "/components/summernote/dist/lang/summernote-en-US.min.js")
          .headers(Headers.headers_12)
          .check(status.is(404)),
        http("request_14")
          .get(uri1 + "/components/awesome-angular-swipe/dist/awesome-angular-swipe.css")
          .headers(Headers.headers_14),
        http("request_15")
          .get(uri1 + "/components/angular-material/modules/js/virtualRepeat/virtualRepeat.min.css")
          .headers(Headers.headers_15),
        http("request_16")
          .get(uri1 + "/components/angular-material/modules/js/fabSpeedDial/fabSpeedDial.min.css")
          .headers(Headers.headers_16),
        http("request_17")
          .get(uri1 + "/linagora.esn.emoticon/js/emoticon-constants.constant.js")
          .headers(Headers.headers_17),
        http("request_18")
          .get(uri1 + "/linagora.esn.emoticon/js/emoticonify.filter.js")
          .headers(Headers.headers_18),
        http("request_19")
          .get(uri1 + "/components/angular-material/modules/js/backdrop/backdrop.min.css")
          .headers(Headers.headers_19),
        http("request_20")
          .get(uri1 + "/components/angular-file-upload/dist/angular-file-upload-shim.min.js")
          .headers(Headers.headers_20),
        http("request_21")
          .get(uri1 + "/components/ui-router-extras/release/ct-ui-router-extras.min.js")
          .headers(Headers.headers_21),
        http("request_22")
          .get(uri1 + "/js/esn/app.js")
          .headers(Headers.headers_22),
        http("request_23")
          .get(uri1 + "/components/mdi/css/materialdesignicons.min.css")
          .headers(Headers.headers_23),
        http("request_24")
          .get(uri1 + "/components/angular-material/modules/js/core/core.min.css")
          .headers(Headers.headers_24),
        http("request_25")
          .get(uri1 + "/components/leaflet/dist/leaflet.css")
          .headers(Headers.headers_25),
        http("request_26")
          .get(uri1 + "/generated/jsApp/esn/app/unifiedinbox")
          .headers(Headers.headers_12),
        http("request_27")
          .get(uri1 + "/components/angular-material/modules/js/button/button.min.css")
          .headers(Headers.headers_27),
        http("request_28")
          .get(uri1 + "/components/angular-material/modules/js/select/select.min.css")
          .headers(Headers.headers_28),
        http("request_29")
          .get(uri1 + "/components/angular-material/modules/js/radioButton/radioButton.min.css")
          .headers(Headers.headers_29),
        http("request_30")
          .get(uri1 + "/generated/jsApp/esn/app/linagora.esn.resource")
          .headers(Headers.headers_12),
        http("request_31")
          .get(uri1 + "/generated/jsApp/esn/app/user-status")
          .headers(Headers.headers_12),
        http("request_32")
          .get(uri1 + "/generated/jsApp/esn/app/lemonldap")
          .headers(Headers.headers_12),
        http("request_33")
          .get(uri1 + "/generated/js/esn/js/import")
          .headers(Headers.headers_12),
        http("request_34")
          .get(uri1 + "/calendar/components/fullcalendar/dist/fullcalendar.css")
          .headers(Headers.headers_34),
        http("request_35")
          .get(uri1 + "/generated/css/esn/styles.css")
          .headers(Headers.headers_35),
        http("request_36")
          .get(uri1 + "/generated/js/esn/js/graceperiod")
          .headers(Headers.headers_12),
        http("request_37")
          .get(uri1 + "/components/angular-material/modules/js/tooltip/tooltip.min.css")
          .headers(Headers.headers_37),
        http("request_38")
          .get(uri1 + "/components/angular-material/modules/js/panel/panel.min.css")
          .headers(Headers.headers_38),
        http("request_39")
          .get(uri1 + "/components/jquery.focus/dist/jquery.focus.min.js")
          .headers(Headers.headers_39),
        http("request_40")
          .get(uri1 + "/components/dynamic-directive/dist/dynamic-directive.min.js")
          .headers(Headers.headers_40),
        http("request_41")
          .get(uri1 + "/generated/jsApp/esn/app/contact")
          .headers(Headers.headers_12),
        http("request_42")
          .get(uri1 + "/generated/js/esn/js/oauth")
          .headers(Headers.headers_12),
        http("request_43")
          .get(uri1 + "/generated/jsApp/esn/app/linagora.esn.dav.import")
          .headers(Headers.headers_12),
        http("request_44")
          .get(uri1 + "/components/material-admin/js/modules/form.js")
          .headers(Headers.headers_44),
        http("request_45")
          .get(uri1 + "/components/angular-material/modules/js/dialog/dialog.min.css")
          .headers(Headers.headers_45),
        http("request_46")
          .get(uri1 + "/components/angular-material/modules/js/icon/icon.min.css")
          .headers(Headers.headers_46),
        http("request_47")
          .get(uri1 + "/components/leaflet/dist/leaflet.js")
          .headers(Headers.headers_47),
        http("request_48")
          .get(uri1 + "/generated/js/esn/js/appstore")
          .headers(Headers.headers_12),
        http("request_49")
          .get(uri1 + "/components/angular-material/modules/js/card/card.min.css")
          .headers(Headers.headers_49),
        http("request_50")
          .get(uri1 + "/generated/jsApp/esn/app/sync")
          .headers(Headers.headers_12),
        http("request_51")
          .get(uri1 + "/generated/js/esn/js/dav")
          .headers(Headers.headers_12),
        http("request_52")
          .get(uri1 + "/generated/js/esn/js/account")
          .headers(Headers.headers_12),
        http("request_53")
          .get(uri1 + "/components/angular-material/modules/js/whiteframe/whiteframe.min.css")
          .headers(Headers.headers_53),
        http("request_54")
          .get(uri1 + "/components/angular-gist/angular-gist.min.js")
          .headers(Headers.headers_54),
        http("request_55")
          .get(uri1 + "/components/twitter-platform-widget/index.js")
          .headers(Headers.headers_55),
        http("request_56")
          .get(uri1 + "/generated/jsApp/esn/app/profile")
          .headers(Headers.headers_12),
        http("request_57")
          .get(uri1 + "/generated/jsApp/esn/app/controlcenter")
          .headers(Headers.headers_12),
        http("request_58")
          .get(uri1 + "/chat/components/angular-inview/angular-inview.js")
          .headers(Headers.headers_58),
        http("request_59")
          .get(uri1 + "/generated/jsApp/esn/app/linagora.esn.unifiedinbox.james")
          .headers(Headers.headers_12),
        http("request_60")
          .get(uri1 + "/linagora.esn.emoticon/js/emoticon.run.js")
          .headers(Headers.headers_60),
        http("request_61")
          .get(uri1 + "/components/jquery/dist/jquery.min.js")
          .headers(Headers.headers_61),
        http("request_62")
          .get(uri1 + "/components/angular-component/dist/angular-component.min.js")
          .headers(Headers.headers_62),
        http("request_63")
          .get(uri1 + "/generated/js/esn/js/contact.google")
          .headers(Headers.headers_12),
        http("request_64")
          .get(uri1 + "/generated/jsApp/esn/app/james")
          .headers(Headers.headers_12),
        http("request_65")
          .get(uri1 + "/generated/js/esn/js/contact.twitter")
          .headers(Headers.headers_12),
        http("request_66")
          .get(uri1 + "/generated/jsApp/esn/app/linagora.esn.unifiedinbox.linshare")
          .headers(Headers.headers_12),
        http("request_67")
          .get(uri1 + "/unifiedinbox/components/angularjs-dragula/dist/dragula.css")
          .headers(Headers.headers_67),
        http("request_68")
          .get(uri1 + "/chat/components/zInfiniteScroll/zInfiniteScroll.js")
          .headers(Headers.headers_68),
        http("request_69")
          .get(uri1 + "/generated/jsApp/esn/app/chat")
          .headers(Headers.headers_12),
        http("request_70")
          .get(uri1 + "/components/lodash/dist/lodash.min.js")
          .headers(Headers.headers_70),
        http("request_71")
          .get(uri1 + "/components/angular/angular.min.js")
          .headers(Headers.headers_71),
        http("request_72")
          .get(uri1 + "/components/bootstrap/dist/js/bootstrap.min.js")
          .headers(Headers.headers_72),
        http("request_73")
          .get(uri1 + "/generated/jsApp/esn/app/contact.collect")
          .headers(Headers.headers_12),
        http("request_74")
          .get(uri1 + "/generated/jsApp/esn/app/linagora.esn.linshare")
          .headers(Headers.headers_12),
        http("request_75")
          .get(uri1 + "/generated/jsApp/esn/app/group")
          .headers(Headers.headers_12),
        http("request_76")
          .get(uri1 + "/generated/jsApp/esn/app/admin")
          .headers(Headers.headers_12),
        http("request_77")
          .get(uri1 + "/components/angular-material/modules/js/menu/menu.min.css")
          .headers(Headers.headers_77),
        http("request_78")
          .get(uri1 + "/generated/jsApp/esn/app/calendar")
          .headers(Headers.headers_12),
        http("request_79")
          .get(uri1 + "/linagora.esn.emoticon/js/emoticon.module.js")
          .headers(Headers.headers_79),
        http("request_80")
          .get(uri1 + "/components/angular-ui-router/release/angular-ui-router.min.js")
          .headers(Headers.headers_80),
        http("request_81")
          .get(uri1 + "/components/angular-sanitize/angular-sanitize.min.js")
          .headers(Headers.headers_81),
        http("request_82")
          .get(uri1 + "/linagora.esn.emoticon/js/list.service.js")
          .headers(Headers.headers_82),
        http("request_83")
          .get(uri1 + "/components/recaptcha_ajax/index.js")
          .headers(Headers.headers_83),
        http("request_84")
          .get(uri1 + "/components/angular-material/modules/js/menuBar/menuBar.min.css")
          .headers(Headers.headers_84),
        http("request_85")
          .get(uri1 + "/components/restangular/dist/restangular.min.js")
          .headers(Headers.headers_85),
        http("request_86")
          .get(uri1 + "/components/angular-promise-extras/angular-promise-extras.js")
          .headers(Headers.headers_86),
        http("request_87")
          .get(uri1 + "/components/angular-recaptcha/release/angular-recaptcha.js")
          .headers(Headers.headers_87),
        http("request_88")
          .get(uri1 + "/components/ngInfiniteScroll/build/ng-infinite-scroll.min.js")
          .headers(Headers.headers_88),
        http("request_89")
          .get(uri1 + "/components/jcrop/js/jquery.Jcrop.min.js")
          .headers(Headers.headers_89),
        http("request_90")
          .get(uri1 + "/components/blueimp-canvas-to-blob/js/canvas-to-blob.min.js")
          .headers(Headers.headers_90),
        http("request_91")
          .get(uri1 + "/components/angular-animate/angular-animate.min.js")
          .headers(Headers.headers_91),
        http("request_92")
          .get(uri1 + "/components/angular-touch/angular-touch.min.js")
          .headers(Headers.headers_92),
        http("request_93")
          .get(uri1 + "/components/angular-strap/dist/angular-strap.min.js")
          .headers(Headers.headers_93),
        http("request_94")
          .get(uri1 + "/generated/js/esn/js/jobqueue")
          .headers(Headers.headers_12),
        http("request_95")
          .get(uri1 + "/components/angular-xeditable/dist/js/xeditable.min.js")
          .headers(Headers.headers_95),
        http("request_96")
          .get(uri1 + "/socket.io/socket.io.js")
          .headers(Headers.headers_96),
        http("request_97")
          .get(uri1 + "/components/angular-strap/dist/angular-strap.tpl.min.js")
          .headers(Headers.headers_97),
        http("request_98")
          .get(uri1 + "/components/angular-truncate/src/truncate.js")
          .headers(Headers.headers_98),
        http("request_99")
          .get(uri1 + "/components/openpaas-logo/openpaas-logo.js")
          .headers(Headers.headers_99),
        http("request_100")
          .get(uri1 + "/components/ng-tags-input/ng-tags-input.min.js")
          .headers(Headers.headers_100),
        http("request_101")
          .get(uri1 + "/components/moment-timezone/builds/moment-timezone-with-data.min.js")
          .headers(Headers.headers_101),
        http("request_102")
          .get(uri1 + "/components/angular-moment/angular-moment.min.js")
          .headers(Headers.headers_102),
        http("request_103")
          .get(uri1 + "/components/angular-file-upload/dist/angular-file-upload.min.js")
          .headers(Headers.headers_103),
        http("request_104")
          .get(uri1 + "/components/moment/min/moment.min.js")
          .headers(Headers.headers_104),
        http("request_105")
          .get(uri1 + "/components/ical.js/build/ical.min.js")
          .headers(Headers.headers_105),
        http("request_106")
          .get(uri1 + "/components/ngGeolocation/ngGeolocation.min.js")
          .headers(Headers.headers_106),
        http("request_107")
          .get(uri1 + "/components/angular-uuid4/angular-uuid4.min.js")
          .headers(Headers.headers_107),
        http("request_108")
          .get(uri1 + "/components/angular-bootstrap-switch/dist/angular-bootstrap-switch.min.js")
          .headers(Headers.headers_108),
        http("request_109")
          .get(uri1 + "/components/angular-leaflet-directive/dist/angular-leaflet-directive.min.js")
          .headers(Headers.headers_109),
        http("request_110")
          .get(uri1 + "/components/bootstrap-switch/dist/js/bootstrap-switch.min.js")
          .headers(Headers.headers_110),
        http("request_111")
          .get(uri1 + "/components/angular-material/modules/js/fabActions/fabActions.min.js")
          .headers(Headers.headers_111),
        http("request_112")
          .get(uri1 + "/components/char-api/lib/charAPI.js")
          .headers(Headers.headers_112),
        http("request_113")
          .get(uri1 + "/components/angular-jstz/angular-jstz.js")
          .headers(Headers.headers_113),
        http("request_114")
          .get(uri1 + "/components/jmap-client/dist/jmap-client.min.js")
          .headers(Headers.headers_114),
        http("request_115")
          .get(uri1 + "/components/Chart.js/Chart.min.js")
          .headers(Headers.headers_115),
        http("request_116")
          .get(uri1 + "/linagora.esn.emoticon/js/categories.service.js")
          .headers(Headers.headers_116),
        http("request_117")
          .get(uri1 + "/components/iframe-resizer/js/iframeResizer.min.js")
          .headers(Headers.headers_117),
        http("request_118")
          .get(uri1 + "/components/showdown/dist/showdown.min.js")
          .headers(Headers.headers_118),
        http("request_119")
          .get(uri1 + "/components/angular-scroll/angular-scroll.min.js")
          .headers(Headers.headers_119),
        http("request_120")
          .get(uri1 + "/components/re-tree/re-tree.min.js")
          .headers(Headers.headers_120),
        http("request_121")
          .get(uri1 + "/components/ng-device-detector/ng-device-detector.min.js")
          .headers(Headers.headers_121),
        http("request_122")
          .get(uri1 + "/components/angular-chart.js/dist/angular-chart.min.js")
          .headers(Headers.headers_122),
        http("request_123")
          .get(uri1 + "/components/angular-markdown-directive/markdown.js")
          .headers(Headers.headers_123),
        http("request_124")
          .get(uri1 + "/components/angular-recursion/angular-recursion.min.js")
          .headers(Headers.headers_124),
        http("request_125")
          .get(uri1 + "/components/localforage/dist/localforage.min.js")
          .headers(Headers.headers_125),
        http("request_126")
          .get(uri1 + "/components/angular-localforage/dist/angular-localForage.min.js")
          .headers(Headers.headers_126),
        http("request_127")
          .get(uri1 + "/components/offline/offline.min.js")
          .headers(Headers.headers_127),
        http("request_128")
          .get(uri1 + "/components/angular-feature-flags/dist/featureFlags.js")
          .headers(Headers.headers_128),
        http("request_129")
          .get(uri1 + "/components/summernote/dist/summernote.min.js")
          .headers(Headers.headers_129),
        http("request_130")
          .get(uri1 + "/components/lng-clockpicker/dist/bootstrap-clockpicker.min.js")
          .headers(Headers.headers_130),
        http("request_131")
          .get(uri1 + "/components/angular-clockpicker/dist/angular-clockpicker.min.js")
          .headers(Headers.headers_131),
        http("request_132")
          .get(uri1 + "/components/arrive/minified/arrive.min.js")
          .headers(Headers.headers_132),
        http("request_133")
          .get(uri1 + "/components/angular-fullscreen/src/angular-fullscreen.js")
          .headers(Headers.headers_133),
        http("request_134")
          .get(uri1 + "/components/angularjs-naturalsort/dist/naturalSortVersion.min.js")
          .headers(Headers.headers_134),
        http("request_135")
          .get(uri1 + "/contact.import.google/js/services.js")
          .headers(Headers.headers_135),
        http("request_136")
          .get(uri1 + "/components/jstzdetect/jstz.min.js")
          .headers(Headers.headers_136),
        http("request_137")
          .get(uri1 + "/components/remarkable-bootstrap-notify/bootstrap-notify.min.js")
          .headers(Headers.headers_137),
        http("request_138")
          .get(uri1 + "/components/autosize/dist/autosize.min.js")
          .headers(Headers.headers_138),
        http("request_139")
          .get(uri1 + "/components/angular-material/modules/js/whiteframe/whiteframe.min.js")
          .headers(Headers.headers_139),
        http("request_140")
          .get(uri1 + "/components/angular-auto-focus/angular-auto-focus.js")
          .headers(Headers.headers_140),
        http("request_141")
          .get(uri1 + "/components/angular-material/modules/js/select/select.min.js")
          .headers(Headers.headers_141),
        http("request_142")
          .get(uri1 + "/components/angular-material/modules/js/menuBar/menuBar.min.js")
          .headers(Headers.headers_142),
        http("request_143")
          .get(uri1 + "/js/constants.js")
          .headers(Headers.headers_143),
        http("request_144")
          .get(uri1 + "/components/angular-summernote/dist/angular-summernote.min.js")
          .headers(Headers.headers_144),
        http("request_145")
          .get(uri1 + "/components/angular-material/modules/js/card/card.min.js")
          .headers(Headers.headers_145),
        http("request_146")
          .get(uri1 + "/components/angular-material/modules/js/radioButton/radioButton.min.js")
          .headers(Headers.headers_146),
        http("request_147")
          .get(uri1 + "/components/matchmedia-ng/matchmedia-ng.js")
          .headers(Headers.headers_147),
        http("request_148")
          .get(uri1 + "/components/matchmedia/matchMedia.js")
          .headers(Headers.headers_148),
        http("request_149")
          .get(uri1 + "/components/waves/dist/waves.min.js")
          .headers(Headers.headers_149),
        http("request_150")
          .get(uri1 + "/components/angular-translate/angular-translate.min.js")
          .headers(Headers.headers_150),
        http("request_151")
          .get(uri1 + "/components/angular-cookies/angular-cookies.min.js")
          .headers(Headers.headers_151),
        http("request_152")
          .get(uri1 + "/components/email-addresses/lib/email-addresses.min.js")
          .headers(Headers.headers_152),
        http("request_153")
          .get(uri1 + "/components/angular-material/modules/js/panel/panel.min.js")
          .headers(Headers.headers_153),
        http("request_154")
          .get(uri1 + "/components/angular-file-saver/dist/angular-file-saver.bundle.min.js")
          .headers(Headers.headers_154),
        http("request_155")
          .get(uri1 + "/components/angular-aria/angular-aria.min.js")
          .headers(Headers.headers_155),
        http("request_156")
          .get(uri1 + "/components/angular-material/modules/js/menu/menu.min.js")
          .headers(Headers.headers_156),
        http("request_157")
          .get(uri1 + "/components/angular-material/modules/js/icon/icon.min.js")
          .headers(Headers.headers_157),
        http("request_158")
          .get(uri1 + "/components/awesome-angular-swipe/lib/awesome-angular-swipe.js")
          .headers(Headers.headers_158),
        http("request_159")
          .get(uri1 + "/components/angular-web-notification/angular-web-notification.js")
          .headers(Headers.headers_159),
        http("request_160")
          .get(uri1 + "/components/angular-material/modules/js/backdrop/backdrop.min.js")
          .headers(Headers.headers_160),
        http("request_161")
          .get(uri1 + "/components/angular-material/modules/js/fabTrigger/fabTrigger.min.js")
          .headers(Headers.headers_161),
        http("request_162")
          .get(uri1 + "/components/angular-material/modules/js/showHide/showHide.min.js")
          .headers(Headers.headers_162),
        http("request_163")
          .get(uri1 + "/components/angular-material/modules/js/tooltip/tooltip.min.js")
          .headers(Headers.headers_163),
        http("request_164")
          .get(uri1 + "/components/angular-material/modules/js/fabSpeedDial/fabSpeedDial.min.js")
          .headers(Headers.headers_164),
        http("request_165")
          .get(uri1 + "/components/angular-material/modules/js/button/button.min.js")
          .headers(Headers.headers_165),
        http("request_166")
          .get(uri1 + "/components/angular-material/modules/js/dialog/dialog.min.js")
          .headers(Headers.headers_166),
        http("request_167")
          .get(uri1 + "/components/angular-material/modules/js/core/core.min.js")
          .headers(Headers.headers_167),
        http("request_168")
          .get(uri1 + "/components/angular-material/modules/js/virtualRepeat/virtualRepeat.min.js")
          .headers(Headers.headers_168),
        http("request_169")
          .get(uri1 + "/components/angular-messages/angular-messages.min.js")
          .headers(Headers.headers_169),
        http("request_170")
          .get(uri1 + "/components/Autolinker.js/dist/Autolinker.min.js")
          .headers(Headers.headers_170),
        http("request_171")
          .get(uri1 + "/components/angular-hotkeys/build/hotkeys.min.js")
          .headers(Headers.headers_171),
        http("request_172")
          .get(uri1 + "/components/html5-desktop-notifications2/dist/Notification.min.js")
          .headers(Headers.headers_172),
        http("request_173")
          .get(uri1 + "/components/angular-i18n/angular-locale_en.js")
          .headers(Headers.headers_173),
        http("request_174")
          .get(uri1 + "/components/angular-scroll-glue/src/scrollglue.js")
          .headers(Headers.headers_174),
        http("request_175")
          .get(uri1 + "/components/summernote/dist/lang/summernote-en-US.min.js")
          .headers(Headers.headers_12)
          .check(status.is(404)),
        http("request_176")
          .get(uri1 + "/components/videogular-overlay-play/vg-overlay-play.min.js")
          .headers(Headers.headers_176),
        http("request_177")
          .get(uri1 + "/contact.import.google/js/app.js")
          .headers(Headers.headers_177),
        http("request_178")
          .get(uri1 + "/components/ngclipboard/dist/ngclipboard.min.js")
          .headers(Headers.headers_178),
        http("request_179")
          .get(uri1 + "/contact.import.google/js/directives.js")
          .headers(Headers.headers_179),
        http("request_180")
          .get(uri1 + "/linagora.esn.emoticon/js/emoticon-popup.component.js")
          .headers(Headers.headers_180),
        http("request_181")
          .get(uri1 + "/js/material.js")
          .headers(Headers.headers_181),
        http("request_182")
          .get(uri1 + "/contact.import.twitter/js/directives.js")
          .headers(Headers.headers_182),
        http("request_183")
          .get(uri1 + "/contact.import.google/js/constants.js")
          .headers(Headers.headers_183),
        http("request_184")
          .get(uri1 + "/contact.import.twitter/js/services.js")
          .headers(Headers.headers_184),
        http("request_185")
          .get(uri1 + "/linagora.esn.emoticon/js/text-transformer.service.js")
          .headers(Headers.headers_185),
        http("request_186")
          .get(uri1 + "/contact.import.twitter/js/app.js")
          .headers(Headers.headers_186),
        http("request_187")
          .get(uri1 + "/contact.import.twitter/js/constants.js")
          .headers(Headers.headers_187),
        http("request_188")
          .get(uri1 + "/js/esn/app.run.js")
          .headers(Headers.headers_188),
        http("request_189")
          .get(uri1 + "/components/clipboard/dist/clipboard.min.js")
          .headers(Headers.headers_189),
        http("request_190")
          .get(uri1 + "/linagora.esn.emoticon/js/emoticon.directive.js")
          .headers(Headers.headers_190),
        http("request_191")
          .get(uri1 + "/components/videogular/videogular.min.js")
          .headers(Headers.headers_191),
        http("request_192")
          .get(uri1 + "/components/angular-sticky/dist/angular-sticky.min.js")
          .headers(Headers.headers_192),
        http("request_193")
          .get(uri1 + "/components/videogular-buffering/vg-buffering.min.js")
          .headers(Headers.headers_193),
        http("request_194")
          .get(uri1 + "/components/videogular-controls/vg-controls.min.js")
          .headers(Headers.headers_194),
        http("request_195")
          .get(uri1 + "/james/components/james-admin-client/dist/james-admin-client.min.js")
          .headers(Headers.headers_195),
        http("request_196")
          .get(uri1 + "/linagora.esn.emoticon/js/emoticon.service.js")
          .headers(Headers.headers_196),
        http("request_197")
          .get(uri1 + "/linagora.esn.linshare/components/linshare-api-client/dist/linshare-api-client.min.js")
          .headers(Headers.headers_197),
        http("request_198")
          .get(uri1 + "/linagora.esn.emoticon/js/emoticon-popup.controller.js")
          .headers(Headers.headers_198),
        http("request_199")
          .get(uri1 + "/unifiedinbox/components/angularjs-dragula/dist/angularjs-dragula.js")
          .headers(Headers.headers_199),
        http("request_200")
          .get(uri1 + "/calendar/components/fullcalendar/dist/locale-all.js")
          .headers(Headers.headers_200),
        http("request_201")
          .get(uri1 + "/calendar/components/fullcalendar/dist/fullcalendar.min.js")
          .headers(Headers.headers_201),
        http("request_202")
          .get(uri1 + "/unifiedinbox/components/sanitize-html/dist/sanitize-html.js")
          .headers(Headers.headers_202),
        http("request_203")
          .get(uri1 + "/api/user/notifications/unread")
          .headers(Headers.headers_203),
        http("request_204")
          .post(uri1 + "/api/configurations?scope=user")
          .headers(Headers.headers_204)
          .body(RawFileBody("OpenPaasLoginLogout_0204_request.txt")),
        http("request_205")
          .get(uri4 + "?origin=https%3A%2F%2Fopenpaas.barcamp.awesomepaas.net&settingsEndpoint=https%3A%2F%2Fsyndication.twitter.com%2Fsettings")
          .headers(Headers.headers_205),
        http("request_206")
          .get(uri1 + "/api/i18n")
          .headers(Headers.headers_206),
        http("request_207")
          .get(uri1 + "/views/commons/loading.html")
          .headers(Headers.headers_207),
        http("request_208")
          .get(uri1 + "/account/api/accounts/providers")
          .headers(Headers.headers_208),
        http("request_209")
          .get(uri1 + "/chat/api/user/privateConversations")
          .headers(Headers.headers_208),
        http("request_210")
          .get(uri1 + "/api/user?_=1548147254727")
          .headers(Headers.headers_210),
        http("request_211")
          .get(uri1 + "/chat/api/user/conversations")
          .headers(Headers.headers_211),
        http("request_212")
          .get(uri2 + "")
          .headers(Headers.headers_212),
        http("request_213")
          .get(uri1 + "/api/domains/5c45c68a45547f0057a07329")
          .headers(Headers.headers_213),
        http("request_214")
          .get(uri1 + "/components/roboto-fontface/fonts/Roboto-Regular.woff2")
          .headers(Headers.headers_214),
        http("request_215")
          .get(uri1 + "/components/roboto-fontface/fonts/Roboto-Medium.woff2")
          .headers(Headers.headers_215),
        http("request_216")
          .get(uri1 + "/api/user/notifications/unread")
          .headers(Headers.headers_203),
        http("request_217")
          .get(uri1 + "/chat/api/user/privateConversations")
          .headers(Headers.headers_208),
        http("request_218")
          .get(uri1 + "/chat/api/user/conversations")
          .headers(Headers.headers_211),
        http("request_219")
          .get(uri1 + "/api/authenticationtoken")
          .headers(Headers.headers_219),
        http("request_220")
          .get(uri1 + "/unifiedinbox/views/home")
          .headers(Headers.headers_220),
        http("request_221")
          .get(uri1 + "/views/esn/partials/application.html")
          .headers(Headers.headers_221),
        http("request_222")
          .get(uri1 + "/chat/api/user/privateConversations")
          .headers(Headers.headers_208),
        http("request_223")
          .get(uri1 + "/socket.io/?token=ccba9e4b-1f04-4ab1-bbcd-c4b6857bea41&user=5c45f2b41d50cf0023d513ad&EIO=3&transport=polling&t=1548147258060-0")
          .headers(Headers.headers_12),
        http("request_224")
          .get(uri1 + "/chat/api/user/conversations")
          .headers(Headers.headers_211),
        http("request_225")
          .get(uri1 + "/unifiedinbox/views/unified-inbox/index")
          .headers(Headers.headers_225),
        http("request_226")
          .get(uri1 + "/views/modules/header/header.html")
          .headers(Headers.headers_226),
        http("request_227")
          .get(uri1 + "/socket.io/?token=ccba9e4b-1f04-4ab1-bbcd-c4b6857bea41&user=5c45f2b41d50cf0023d513ad&EIO=3&transport=polling&t=1548147258111-1&sid=4Q9s4SNCtCWeJVYSAAAO")
          .headers(Headers.headers_12),
        http("request_228")
          .get(uri1 + "/unifiedinbox/app/components/banner/quota-banner/quota-banner.html")
          .headers(Headers.headers_228),
        http("request_229")
          .get(uri1 + "/unifiedinbox/views/partials/empty-messages/inbox-list-account-unavailable.html")
          .headers(Headers.headers_229),
        http("request_230")
          .get(uri1 + "/unifiedinbox/app/components/banner/vacation-banner/vacation-banner.html")
          .headers(Headers.headers_230),
        http("request_231")
          .get(uri1 + "/views/modules/ui/fab.html")
          .headers(Headers.headers_231),
        http("request_232")
          .get(uri1 + "/unifiedinbox/app/components/list/header/list-header.html")
          .headers(Headers.headers_232),
        http("request_233")
          .get(uri1 + "/unifiedinbox/views/unified-inbox/subheader.html")
          .headers(Headers.headers_233),
        http("request_234")
          .post(uri1 + "/api/jwt/generate")
          .headers(Headers.headers_234),
        http("request_235")
          .post(uri1 + "/api/jwt/generate")
          .headers(Headers.headers_234),
        http("request_236")
          .post(uri1 + "/api/jwt/generate")
          .headers(Headers.headers_234),
        http("request_237")
          .get(uri1 + "/unifiedinbox/views/partials/empty-messages/index.html")
          .headers(Headers.headers_237),
        http("request_238")
          .get(uri1 + "/unifiedinbox/views/partials/inbox-fab.html")
          .headers(Headers.headers_238),
        http("request_239")
          .get(uri1 + "/views/modules/user-notification/toggler/user-notification-toggler.html")
          .headers(Headers.headers_239),
        http("request_240")
          .get(uri1 + "/unifiedinbox/views/sidebar/sidebar-menu.html")
          .headers(Headers.headers_240),
        http("request_241")
          .get(uri1 + "/views/modules/search/header/search-header.html")
          .headers(Headers.headers_241),
        http("request_242")
          .get(uri1 + "/views/modules/header/profile-menu/profile-menu.html")
          .headers(Headers.headers_242),
        http("request_243")
          .get(uri1 + "/views/modules/subheader/sub-header-container.html")
          .headers(Headers.headers_243),
        http("request_244")
          .get(uri1 + "/views/modules/application-menu/application-menu-toggler.html")
          .headers(Headers.headers_244),
        http("request_245")
          .post(uri1 + "/socket.io/?token=ccba9e4b-1f04-4ab1-bbcd-c4b6857bea41&user=5c45f2b41d50cf0023d513ad&EIO=3&transport=polling&t=1548147258323-2&sid=4Q9s4SNCtCWeJVYSAAAO")
          .headers(Headers.headers_245),
        http("request_246")
          .get(uri1 + "/images/white-logo.svg")
          .headers(Headers.headers_246),
        http("request_247")
          .get(uri1 + "/socket.io/?token=ccba9e4b-1f04-4ab1-bbcd-c4b6857bea41&user=5c45f2b41d50cf0023d513ad&EIO=3&transport=polling&t=1548147258326-3&sid=4Q9s4SNCtCWeJVYSAAAO")
          .headers(Headers.headers_12),
        http("request_248")
          .get(uri1 + "/components/mdi/fonts/materialdesignicons-webfont.woff2?v=2.0.46")
          .headers(Headers.headers_248),
        http("request_249")
          .post(uri1 + "/api/jwt/generate")
          .headers(Headers.headers_234),
        http("request_250")
          .get(uri1 + "/unifiedinbox/views/partials/inbox-home-button.html")
          .headers(Headers.headers_250),
        http("request_251")
          .post(uri1 + "/api/jwt/generate")
          .headers(Headers.headers_234),
        http("request_252")
          .options(uri5 + "")
          .headers(Headers.headers_252)
          .check(status.is(502)),
        http("request_253")
          .get(uri1 + "/unifiedinbox/app/components/list/group-toggle-selection/list-group-toggle-selection.html")
          .headers(Headers.headers_253),
        http("request_254")
          .get(uri1 + "/unifiedinbox/app/components/list/refresh-button/refresh-button.html")
          .headers(Headers.headers_254),
        http("request_255")
          .options(uri5 + "")
          .headers(Headers.headers_252)
          .check(status.is(502)),
        http("request_256")
          .options(uri5 + "")
          .headers(Headers.headers_252)
          .check(status.is(502)),
        http("request_257")
          .get(uri1 + "/unifiedinbox/views/partials/subheader/burger-button.html")
          .headers(Headers.headers_257),
        http("request_258")
          .get(uri1 + "/unifiedinbox/app/components/filter-input/filter-input.html")
          .headers(Headers.headers_258),
        http("request_259")
          .get(uri1 + "/unifiedinbox/app/components/subheader/delete-button/subheader-delete-button.html")
          .headers(Headers.headers_259),
        http("request_260")
          .get(uri1 + "/unifiedinbox/app/components/subheader/mark-as-read-button/subheader-mark-as-read-button.html")
          .headers(Headers.headers_260),
        http("request_261")
          .get(uri1 + "/unifiedinbox/app/components/subheader/more-button/subheader-more-button.html")
          .headers(Headers.headers_261),
        http("request_262")
          .get(uri1 + "/unifiedinbox/views/filter/filter-button.html")
          .headers(Headers.headers_262),
        http("request_263")
          .get(uri1 + "/unifiedinbox/app/components/sidebar/attachment-button/sidebar-attachment-button.html")
          .headers(Headers.headers_263),
        http("request_264")
          .get(uri1 + "/unifiedinbox/app/components/subheader/not-spam-button/subheader-not-spam-button.html")
          .headers(Headers.headers_264),
        http("request_265")
          .get(uri1 + "/unifiedinbox/views/filter/inbox-clear-filters-button.html")
          .headers(Headers.headers_265),
        http("request_266")
          .get(uri1 + "/views/modules/user-notification/popover/user-notification-popover.html")
          .headers(Headers.headers_266),
        http("request_267")
          .get(uri1 + "/unifiedinbox/views/sidebar/email/menu.html")
          .headers(Headers.headers_267),
        http("request_268")
          .get(uri1 + "/unifiedinbox/views/sidebar/configuration/configuration-button.html")
          .headers(Headers.headers_268),
        http("request_269")
          .get(uri1 + "/unifiedinbox/app/components/sidebar/unifiedinbox-button/unifiedinbox-button.html")
          .headers(Headers.headers_269),
        http("request_270")
          .get(uri1 + "/components/roboto-fontface/fonts/Roboto-RegularItalic.woff2")
          .headers(Headers.headers_270),
        http("request_271")
          .get(uri1 + "/components/roboto-fontface/fonts/Roboto-Bold.woff2")
          .headers(Headers.headers_271),
        http("request_272")
          .get(uri1 + "/controlcenter/app/profile-menu-controlcenter/profile-menu-controlcenter.html")
          .headers(Headers.headers_272),
        http("request_273")
          .get(uri1 + "/views/modules/search/form/advanced/search-advanced-form.html")
          .headers(Headers.headers_273),
        http("request_274")
          .get(uri1 + "/unifiedinbox/app/components/user-quota/user-quota.html")
          .headers(Headers.headers_274),
        http("request_275")
          .options(uri5 + "")
          .headers(Headers.headers_252)
          .check(status.is(502)),
        http("request_276")
          .options(uri5 + "")
          .headers(Headers.headers_252)
          .check(status.is(502)),
        http("request_277")
          .get(uri1 + "/profile/app/profile-menu-profile/profile-menu-profile.html")
          .headers(Headers.headers_277),
        http("request_278")
          .get(uri1 + "/unifiedinbox/app/components/sidebar/social-networks/social-networks.html")
          .headers(Headers.headers_278),
        http("request_279")
          .get(uri1 + "/views/modules/application-menu/application-menu.html")
          .headers(Headers.headers_279),
        http("request_280")
          .options(uri5 + "")
          .headers(Headers.headers_252)
          .check(status.is(502)),
        http("request_281")
          .get(uri1 + "/views/modules/sidebar/contextual-sidebar.html")
          .headers(Headers.headers_281),
        http("request_282")
          .get(uri1 + "/unifiedinbox/views/components/sidebar/attachment/sidebar-attachment.html")
          .headers(Headers.headers_282),
        http("request_283")
          .get(uri1 + "/views/modules/user-notification/list/user-notification-list.html")
          .headers(Headers.headers_283),
        http("request_284")
          .get(uri1 + "/api/user/profile/avatar?cb=1548147258543")
          .headers(Headers.headers_284),
        http("request_285")
          .get(uri1 + "/unifiedinbox/views/sidebar/email/menu-item.html")
          .headers(Headers.headers_285),
        http("request_286")
          .get(uri1 + "/api/avatars?objectType=email&email=${user}")
          .headers(Headers.headers_286),
        http("request_287")
          .options(uri5 + "")
          .headers(Headers.headers_252)
          .check(status.is(502)),
        http("request_288")
          .options(uri5 + "")
          .headers(Headers.headers_252)
          .check(status.is(502)),
        http("request_289")
          .get(uri1 + "/views/modules/search/form/advanced/search-advanced-toggle-button.html")
          .headers(Headers.headers_289),
        http("request_290")
          .get(uri1 + "/views/modules/search/provider-select/search-provider-select.html")
          .headers(Headers.headers_290),
        http("request_291")
          .get(uri1 + "/unifiedinbox/views/partials/empty-messages/containers/inbox.html")
          .headers(Headers.headers_291),
        http("request_292")
          .get(uri1 + "/unifiedinbox/app/components/sidebar/social-networks-settings/social-networks-settings.html")
          .headers(Headers.headers_292),
        http("request_293")
          .get(uri1 + "/views/modules/settings-overlay/template.html")
          .headers(Headers.headers_293),
        http("request_294")
          .get(uri1 + "/images/logo-tiny.png")
          .headers(Headers.headers_294),
        http("request_295")
          .get(uri1 + "/views/modules/attachment/list/attachment-list.html")
          .headers(Headers.headers_295),
        http("request_296")
          .get(uri1 + "/views/modules/infinite-list/infinite-list.html")
          .headers(Headers.headers_296),
        http("request_297")
          .get(uri1 + "/unifiedinbox/app/components/sidebar/folder-settings/folder-settings.html")
          .headers(Headers.headers_297),
        http("request_298")
          .get(uri1 + "/images/mdi/mdi.svg")
          .headers(Headers.headers_298),
        http("request_299")
          .get(uri1 + "/views/modules/attachment/list/item/attachment-list-item.html")
          .headers(Headers.headers_299))

    val inbox = http("request_300")
      .options(uri5 + "")
      .headers(Headers.headers_252)
      .resources(http("request_301")
        .get(uri1 + "/logout")
        .headers(Headers.headers_301),
        http("request_302")
          .get("/skins/common/js/jquery-1.10.2.min.js"),
        http("request_303")
          .get("/skins/common/js/jquery-ui-1.10.3.custom.min.js"),
        http("request_304")
          .get("/skins/common/js/jquery.base64.min.js"),
        http("request_305")
          .get("/skins/common/js/jquery.cookie.min.js"),
        http("request_306")
          .get("/skins/linagora/js/skin.min.js"),
        http("request_307")
          .get("/skins/common/js/portal.min.js"),
        http("request_308")
          .get("/skins/linagora/js/bootstrap.min.js"),
        http("request_309")
          .get("/skins/linagora/css/bootstrap-theme.min.css")
          .headers(Headers.headers_309),
        http("request_310")
          .get("/skins/linagora/css/custom-op-lemon.css")
          .headers(Headers.headers_309),
        http("request_311")
          .get("/skins/linagora/css/roboto-fontface/css/roboto-fontface.css")
          .headers(Headers.headers_309),
        http("request_312")
          .get("/skins/common/js/info.min.js")
          .headers(Headers.headers_309),
        http("request_313")
          .get("/skins/linagora/css/animate.css/animate.min.css")
          .headers(Headers.headers_309),
        http("request_314")
          .get("/skins/linagora/css/mdi/css/materialdesignicons.min.css")
          .headers(Headers.headers_309),
        http("request_315")
          .get("/skins/linagora/css/bootstrap.min.css")
          .headers(Headers.headers_309),
        http("request_316")
          .get("/skins/linagora/css/custom-openpaas.css")
          .headers(Headers.headers_309),
        http("request_317")
          .get("/skins/linagora/fonts/glyphicons-halflings-regular.woff"),
        http("request_318")
          .get("/skins/linagora/css/mdi/fonts/materialdesignicons-webfont.woff2?v=2.0.46"),
        http("request_319")
          .get("/skins/linagora/css/roboto-fontface/fonts/Roboto-Regular.woff2"),
        http("request_320")
          .get("/skins/linagora/css/roboto-fontface/fonts/Roboto-Medium.woff2"),
        http("request_321")
          .get("/skins/linagora/css/roboto-fontface/fonts/Roboto-Bold.woff2"),
        http("request_322")
          .get("/skins/linagora/images/login-1366.jpg"),
        http("request_323")
          .get("/skins/linagora/images/white-logo.svg"),
        http("request_324")
          .get("/components/bootstrap/dist/fonts/glyphicons-halflings-regular.woff2")
          .headers(Headers.headers_324)
          .check(status.is(404)),
        http("request_325")
          .get("/components/bootstrap/dist/fonts/glyphicons-halflings-regular.woff")
          .headers(Headers.headers_324)
          .check(status.is(404)),
        http("request_326")
          .get("/components/bootstrap/dist/fonts/glyphicons-halflings-regular.ttf")
          .headers(Headers.headers_324)
          .check(status.is(404)))
      .check(status.is(502))

    val logout = http("request_327")
      .get("/?logout=1&skin=linagora")
      .headers(Headers.headers_301)
      .resources(http("request_328")
        .get("/skins/linagora/fonts/glyphicons-halflings-regular.woff"),
        http("request_329")
          .get("/skins/linagora/css/mdi/fonts/materialdesignicons-webfont.woff2?v=2.0.46"),
        http("request_330")
          .get("/skins/linagora/css/roboto-fontface/fonts/Roboto-Regular.woff2"),
        http("request_331")
          .get("/skins/linagora/css/roboto-fontface/fonts/Roboto-Medium.woff2"),
        http("request_332")
          .get("/skins/linagora/css/roboto-fontface/fonts/Roboto-Bold.woff2"),
        http("request_333")
          .get("/skins/linagora/images/login-1366.jpg"),
        http("request_334")
          .get("/skins/linagora/images/white-logo.svg"),
        http("request_335")
          .get("/components/bootstrap/dist/fonts/glyphicons-halflings-regular.woff2")
          .headers(Headers.headers_324)
          .check(status.is(404)),
        http("request_336")
          .get("/components/bootstrap/dist/fonts/glyphicons-halflings-regular.woff")
          .headers(Headers.headers_324)
          .check(status.is(404)),
        http("request_337")
          .get("/components/bootstrap/dist/fonts/glyphicons-halflings-regular.ttf")
          .headers(Headers.headers_324)
          .check(status.is(404)))

}
