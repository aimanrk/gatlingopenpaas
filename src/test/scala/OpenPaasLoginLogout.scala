
import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class OpenPaasLoginLogout extends Simulation {

  val usersFeeder = csv("users.csv").circular

	val httpProtocol = http
		.baseUrl("https://auth.barcamp.awesomepaas.net")
		.inferHtmlResources()


	val scn = scenario("OpenPaasLoginLogout")
    .feed(usersFeeder)
		.exec(Requests.login)
		.pause(12)
		.exec(Requests.inbox)
		.pause(9)
		.exec(Requests.logout)

	setUp(scn.inject(atOnceUsers(1))).protocols(httpProtocol)
}